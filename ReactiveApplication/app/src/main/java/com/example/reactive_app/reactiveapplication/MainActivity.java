package com.example.reactive_app.reactiveapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.reactive_app.reactiveapplication.observable.Binding;
import com.example.reactive_app.reactiveapplication.observable.DataFactory;
import com.example.reactive_app.reactiveapplication.observable.DataFactory.Mapping2;
import com.example.reactive_app.reactiveapplication.observable.ObservableData;
import com.example.reactive_app.reactiveapplication.observable.databinding.DataBinder;
import com.example.reactive_app.reactiveapplication.observable.databinding.DataBinder.Updater;
import com.example.reactive_app.reactiveapplication.observable.impl.AbstractObservableData;
import com.example.reactive_app.reactiveapplication.observable.impl.BindingList;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    private Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ------------------------------------------------------------------------------
        // get views
        // ------------------------------------------------------------------------------
        EditText nameEditText = (EditText)findViewById(R.id.nameEditText);
        DatePicker birthdayDatePicker = (DatePicker)findViewById(R.id.birthdayDatePicker);
        final TextView messageTextView = (TextView)findViewById(R.id.messageTextView);

        // ------------------------------------------------------------------------------
        // basic observables
        // ------------------------------------------------------------------------------
        ObservableData<CharSequence> nameObservable = asObservable(nameEditText);
        ObservableData<Calendar> birthdayObservable = asObservable(birthdayDatePicker, 2000, 1, 1);
        ObservableData<Calendar> currentDateObservable = createCurrentDateObservable();

        // ------------------------------------------------------------------------------
        // derived observables
        // ------------------------------------------------------------------------------
        ObservableData<Integer> ageObservable = DataFactory.map2(birthdayObservable, currentDateObservable, new Mapping2<Calendar, Calendar, Integer>() {
            @Override
            public Integer map(Calendar birthday, Calendar currentDate) {
                return calculateAgeInYears(birthday, currentDate);
            }
        });
        ObservableData<String> messageObservable = DataFactory.map2(nameObservable, ageObservable, new Mapping2<CharSequence, Integer, String>() {
            @Override
            public String map(CharSequence name, Integer age) {
                return name.length() > 0 ? "Hello " + name + ". You are " + age + " year(s) old." : "";
            }
        });

        // ------------------------------------------------------------------------------
        // bind result to view
        // ------------------------------------------------------------------------------
        binding = DataBinder.bind(messageObservable, new Updater<String>() {
            @Override
            public void update(String value) {
                messageTextView.setText(value);
            }
        });
    }

    @Override
    protected void onDestroy() {
        binding.close();
        super.onDestroy();
    }

    private static ObservableData<CharSequence> asObservable(final EditText editText) {
        final AbstractObservableData<CharSequence> observableData = new AbstractObservableData<CharSequence>() {
            @Override
            public CharSequence getValue() {
                return editText.getText();
            }
        };

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                observableData.notifyObservers();
            }
        });

        return observableData;
    }

    private static ObservableData<Calendar> asObservable(final DatePicker datePicker, int year, int month, int dayOfMonth) {
        final AbstractObservableData<Calendar> observableData = new AbstractObservableData<Calendar>() {
            @Override
            public Calendar getValue() {
                return new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
            }
        };

        datePicker.init(year, month, dayOfMonth, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                observableData.notifyObservers();
            }
        });

        return observableData;
    }

    private static ObservableData<Calendar> createCurrentDateObservable() {
        // no live update yet

        return new AbstractObservableData<Calendar>() {
            @Override
            public Calendar getValue() {
                return new GregorianCalendar();
            }
        };
    }

    private static int calculateAgeInYears(Calendar birthday, Calendar currentDate) {
        int ageInYears = 0;
        Calendar date = (Calendar)currentDate.clone();

        if (date.before(birthday)) {
            while (true) {
                date.roll(Calendar.YEAR, true); // increment
                if (date.after(birthday)) {
                    break;
                }
                ageInYears--;
            }
        } else if (date.after(birthday)) {
            while (true) {
                date.roll(Calendar.YEAR, false); // decrement
                if (date.before(birthday)) {
                    break;
                }
                ageInYears++;
            }
        }

        return ageInYears;
    }
}
