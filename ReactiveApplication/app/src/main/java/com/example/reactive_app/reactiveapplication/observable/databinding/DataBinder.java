/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable.databinding;

import com.example.reactive_app.reactiveapplication.Preconditions;
import com.example.reactive_app.reactiveapplication.observable.DataFactory;
import com.example.reactive_app.reactiveapplication.observable.Binding;
import com.example.reactive_app.reactiveapplication.observable.ObservableData;
import com.example.reactive_app.reactiveapplication.observable.Observer;

public class DataBinder {
    /**
     * Interface for updating a value on an object (although the reference on it has to be provided by the implementor of the
     * interface). It is guaranteed to be called at the beginning for initialization and then on every update. Updates with the same
     * value as before aren't filtered.
     * 
     * @param <T>
     */
    public static interface Updater<T> {
        public void update(T value);
    }

    /**
     * Same as {@link Updater <T>} but for updates depending on two values.
     *
     * @param <T1>
     * @param <T2>
     */
    public static interface Updater2<T1, T2> {
        public void update(T1 value1, T2 value2);
    }

    /**
     * Same as {@link Updater <T>} but for updates depending on three values.
     *
     * @param <T1>
     * @param <T2>
     * @param <T3>
     */
    public static interface Updater3<T1, T2, T3> {
        public void update(T1 value1, T2 value2, T3 value3);
    }

    public static <T> Binding bind(final ObservableData<? extends T> data, final Updater<? super T> updater) {
        Preconditions.checkNotNull(data);
        Preconditions.checkNotNull(updater);

        Binding binding = data.observe(new Observer() {

            @Override
            public void update() {
                updater.update(data.getValue());
            }
        });

        // NOTE: the Updater is called at the beginning to do initialization
        //
        // We do this after adding the observer to ensure we don't miss any update. On the other hand there's a small chance that
        // the Updater is called twice with the same value because we do initialization after already an update has happen.
        updater.update(data.getValue());

        return binding;
    }

    /**
     * Same as {@link DataBinder#bind(ObservableData, Updater)} but for bindings on two values at the same time.
     * 
     * @param data1
     * @param data2
     * @param updater
     * @return
     */
    public static <T1, T2> Binding bind2(final ObservableData<? extends T1> data1, final ObservableData<? extends T2> data2,
            final Updater2<? super T1, ? super T2> updater) {
        Preconditions.checkNotNull(data1);
        Preconditions.checkNotNull(data2);
        Preconditions.checkNotNull(updater);

        Binding binding = DataFactory.observeMany(new ObservableData<?>[] {
                data1, data2
        }, new Observer() {
            @Override
            public void update() {
                updater.update(data1.getValue(), data2.getValue());
            }
        });
        updater.update(data1.getValue(), data2.getValue());

        /**
         * See comments in {@link #bind(...)}.
         */

        return binding;
    }

    /**
     * Same as {@link DataBinder#bind(ObservableData, Updater)} but for bindings on three values at the same time.
     * 
     * @param data1
     * @param data2
     * @param data3
     * @param updater
     * @return
     */
    public static <T1, T2, T3> Binding bind3(final ObservableData<? extends T1> data1, final ObservableData<? extends T2> data2,
            final ObservableData<? extends T3> data3, final Updater3<? super T1, ? super T2, ? super T3> updater) {
        Preconditions.checkNotNull(data1);
        Preconditions.checkNotNull(data2);
        Preconditions.checkNotNull(data3);
        Preconditions.checkNotNull(updater);

        Binding binding = DataFactory.observeMany(new ObservableData<?>[] {
                data1, data2, data3
        }, new Observer() {
            @Override
            public void update() {
                updater.update(data1.getValue(), data2.getValue(), data3.getValue());
            }
        });
        updater.update(data1.getValue(), data2.getValue(), data3.getValue());

        /**
         * See comments in {@link #bind(...)}.
         */

        return binding;
    }
}
