/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication;

import android.os.Looper;

public final class Preconditions {

    public static class PreconditionViolationException extends RuntimeException {
        private static final long serialVersionUID = 1L;
    }

    public static class WrongThreadException extends RuntimeException {
        private static final long serialVersionUID = 1L;
    }

    /**
     * Utility method to check a condition for <code>true</code>.
     * 
     * @param value
     */
    public static void check(boolean conditionBeingTrue) {
        if (!conditionBeingTrue) {
            throw new PreconditionViolationException();
        }
    }

    /**
     * Utility method to check a reference for not null. It can be used either as statement or within an expression.
     * 
     * @param reference Reference to be checked
     * @return The reference (as given as argument).
     * @throws NullPointerExcpetion if reference is null.
     */
    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }

        return reference;
    }

    /**
     * Utility method to check if we are running on the main thread.
     */
    public static void runningOnMainThread() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            throw new WrongThreadException();
        }
    }
}
