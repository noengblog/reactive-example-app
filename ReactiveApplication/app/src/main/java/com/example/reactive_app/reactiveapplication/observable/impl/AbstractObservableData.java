/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable.impl;

import com.example.reactive_app.reactiveapplication.observable.Binding;
import com.example.reactive_app.reactiveapplication.observable.ObservableData;
import com.example.reactive_app.reactiveapplication.observable.Observer;

/**
 * Basic read-only implementation of ObservableData already including the observer management (but without get value). It provides
 * a method to notify observers explicitly allowing bridging to other asynchronous mechanisms.
 * 
 * @param <T>
 */
public abstract class AbstractObservableData<T> implements ObservableData<T> {

    private ObservableImpl observable = new ObservableImpl();

    @Override
    public Binding observe(Observer observer) {
        return observable.observe(observer);
    }

    public void notifyObservers() {
        observable.notifyObservers();
    }
}
