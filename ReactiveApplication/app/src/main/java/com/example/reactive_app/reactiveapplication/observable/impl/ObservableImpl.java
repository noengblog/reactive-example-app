/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable.impl;

import java.util.concurrent.CopyOnWriteArrayList;

import com.example.reactive_app.reactiveapplication.observable.Binding;
import com.example.reactive_app.reactiveapplication.observable.Observable;
import com.example.reactive_app.reactiveapplication.observable.Observer;

/**
 * Helper class providing the observer management to implement the observable part of the <code>ObservableData&lt;T&gt;</code>
 * interface. It is thread safe.
 */
public class ObservableImpl implements Observable {
    // We use CopyOnWriteArrayList instead of ArrayList to allow adding/removing observers (by the same thread) while iterating
    // through the observer list.
    private CopyOnWriteArrayList<Observer> observers = new CopyOnWriteArrayList<Observer>();

    public Binding observe(final Observer observer) {
        synchronized (observers) {
            observers.add(observer);
        }

        return new Binding() {
            @Override
            public void close() {
                synchronized (observers) {
                    observers.remove(observer);
                }
            }
        };
    }

    public void notifyObservers() {
        synchronized (observers) {
            for (Observer observer : observers) {
                observer.update();
            }
        }
    }
}
