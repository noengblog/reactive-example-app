/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable;

import java.lang.reflect.Field;

import android.os.Handler;

import com.example.reactive_app.reactiveapplication.Preconditions;
import com.example.reactive_app.reactiveapplication.observable.databinding.DataBinder;
import com.example.reactive_app.reactiveapplication.observable.databinding.DataBinder.Updater;
import com.example.reactive_app.reactiveapplication.observable.impl.AbstractReferenceCountedObservable;
import com.example.reactive_app.reactiveapplication.observable.impl.BindingList;
import com.example.reactive_app.reactiveapplication.observable.impl.ObservableImpl;
import com.example.reactive_app.reactiveapplication.threading.ThreadDelegation;

public class DataFactory {
    /**
     * Create local data (not bound to anything else).
     * 
     * @param initialValue Initial value
     * @return Data object
     */
    public static <T> WritableData<T> create(final T initialValue) {
        return new WritableData<T>() {
            private T value = initialValue;

            @Override
            public T getValue() {
                return value;
            }

            @Override
            public void setValue(T value) {
                this.value = value;
            }
        };
    }

    /**
     * Create local observable data (not bound to anything else).
     * 
     * @param initialValue Initial value
     * @return IDataObservable object
     */
    public static <T> ObservableWritableData<T> createObservable(final T initialValue) {
        return toObservable(create(initialValue));
    }

    /**
     * Create an observable wrapper around an {@link WritableData} object assuming that the data isn't modified elsewhere directly.
     * The observers are notified on each <code>setValue</code> call.<br/>
     * The observers are called in the context of the <code>setValue</code> caller.
     * 
     * @param <T>
     */
    public static <T> ObservableWritableData<T> toObservable(final WritableData<T> data) {
        return new ObservableWritableData<T>() {
            private ObservableImpl observable = new ObservableImpl();

            @Override
            public T getValue() {
                return data.getValue();
            }

            @Override
            public void setValue(T value) {
                data.setValue(value);

                observable.notifyObservers();
            }

            @Override
            public Binding observe(Observer observer) {
                return observable.observe(observer);
            }
        };
    }

    public static enum FieldCannotSetNullAction {
        IGNORE, EXCEPTION
    }

    public static <T> WritableData<T> wrapField(final Object object, Class<T> fieldType, String fieldName) {
        return wrapField(object, fieldType, fieldName, FieldCannotSetNullAction.EXCEPTION);
    }

    public static <T> WritableData<T> wrapField(final Object object, Class<T> fieldType, String fieldName,
            final FieldCannotSetNullAction nullFailureAction) {
        Preconditions.checkNotNull(object);
        Preconditions.checkNotNull(fieldType);
        Preconditions.checkNotNull(fieldName);
        Preconditions.checkNotNull(nullFailureAction);

        final Field field = getField(object.getClass(), fieldName);
        if (field == null) {
            // programming error: not a checked exception, let it crash
            throw new RuntimeException("Field " + fieldName + " not found in " + object.getClass().getName());
        }
        if (field.getType().isPrimitive() != fieldType.isPrimitive()) {
            // programming error: not a checked exception, let it crash
            throw new RuntimeException(
                    "Specified field type and actual field type have to be either both primitives or both non-primitives");
        }
        if (!field.getType().equals(fieldType)) {
            // programming error: not a checked exception, let it crash
            throw new RuntimeException("Field " + fieldName + " has wrong type: " + field.getType().getName());
        }
        field.setAccessible(true);

        final boolean isPrimitive = fieldType.getClass().isPrimitive();

        return new WritableData<T>() {
            @SuppressWarnings("unchecked")
            @Override
            public T getValue() {
                try {
                    return (T) field.get(object);
                } catch (IllegalArgumentException e) {
                    throw new RuntimeException(e); // we don't expect this (actual object type has been used to get the field)
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e); // we don't expect this (accessibility checked before)
                }
            }

            @Override
            public void setValue(T value) {
                if (isPrimitive && (value == null)) {
                    switch (nullFailureAction) {
                        case IGNORE:
                            return;

                        case EXCEPTION:
                            throw new NullPointerException();
                    }
                }

                try {
                    field.set(object, value);
                } catch (IllegalArgumentException e) {
                    throw new RuntimeException(e); // we don't expect this (type statically checked)
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e); // we don't expect this (accessibility checked before)
                }
            }
        };
    }

    private static Field getField(Class<?> type, String fieldName) {
        try {
            return type.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            if (type.getSuperclass() != null) {
                return getField(type.getSuperclass(), fieldName);
            } else {
                return null;
            }
        }
    }

    /**
     * Wrapper around an {@link Observable} object that forwards observer calls to the context of a given handler.
     * <p>
     * Special care has been taken that no observer calls are done anymore right after removing the binding.
     */
    private static class HandlerDelegatedObservable implements Observable {
        private final Observable observable;

        private Handler handler;

        public HandlerDelegatedObservable(Observable observable, Handler handler) {
            this.observable = observable;
            this.handler = handler;
        }

        private class HandlerDelegatedObserver implements Observer {
            public volatile boolean added = true;

            private Observer innerObserver;

            public HandlerDelegatedObserver(Observer innerObserver) {
                this.innerObserver = innerObserver;
            }

            @Override
            public void update() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (HandlerDelegatedObserver.this) {
                            if (added) { // precaution: don't call this anymore if the binding has been removed after posting
                                innerObserver.update();
                            }
                        }
                    }
                });
            }
        }

        @Override
        public Binding observe(Observer observer) {
            final HandlerDelegatedObserver handlerDelegatedObserver = new HandlerDelegatedObserver(observer);
            final Binding innerBinding = observable.observe(handlerDelegatedObserver);

            return new Binding() {
                @Override
                public void close() {
                    innerBinding.close();
                    synchronized (handlerDelegatedObserver) {
                        handlerDelegatedObserver.added = false;
                    }
                }
            };
        }
    }

    /**
     * Extension of {@link HandlerDelegatedObservable} for observable data.
     */
    private static class HandlerDelegatedObservableData<T> extends HandlerDelegatedObservable implements ObservableData<T> {
        private final ObservableData<T> data;

        public HandlerDelegatedObservableData(ObservableData<T> data, Handler handler) {
            super(data, handler);
            this.data = data;
        }

        @Override
        public T getValue() {
            return data.getValue();
        }
    }

    /**
     * Create a wrapper around an {@link ObservableData} object that forwards observer calls to the main thread.
     * <p>
     * Special care has been taken that no observer calls are done anymore right after removing the binding.
     */
    public static Observable notifiedOnMainThread(final Observable observable) {
        if (isAlreadyNotifiedOnMainThread(observable)) {
            return observable;
        }

        return new HandlerDelegatedObservable(observable, ThreadDelegation.getMainThreadHandler());
    }

    /**
     * Create a wrapper around an {@link ObservableData} object that forwards observer calls to the main thread.
     * <p>
     * Special care has been taken that no observer calls are done anymore right after removing the binding.
     */
    public static <T> ObservableData<T> notifiedOnMainThread(final ObservableData<T> data) {
        if (isAlreadyNotifiedOnMainThread(data)) {
            return data;
        }

        return new HandlerDelegatedObservableData<T>(data, ThreadDelegation.getMainThreadHandler());
    }

    private static boolean isAlreadyNotifiedOnMainThread(final Observable observable) {
        if (observable instanceof HandlerDelegatedObservable) {
            HandlerDelegatedObservable concreteObservable = (HandlerDelegatedObservable) observable;
            return ThreadDelegation.isHandlerOnMainThread(concreteObservable.handler);
        }
        return false;
    }

    public static Binding observeMany(ObservableData<?>[] observables, final Observer observer) {
        Observer combinedObserver = new Observer() {
            @Override
            public void update() {
                observer.update();
            }
        };

        BindingList bindings = new BindingList();
        for (ObservableData<?> observable : observables) {
            bindings.add(observable.observe(combinedObserver));
        }

        return bindings;
    }

    /**
     * Wrapper for <code>ObservableData</code> that caches value updates (call of <code>getValue</code> right after notifying the
     * observers)
     */
    private static class CachedObservableData<T> implements ObservableData<T> {
        private ObservableData<T> data;

        private volatile Binding binding;

        private volatile boolean initialized = false;

        private T value;

        public CachedObservableData(ObservableData<T> data) {
            this.data = data;
        }

        /**
         * Reference counting is used to prevent reference leaks: This wrapper is only registered as observer on the data if any
         * observers are registered to this wrapper.
         */
        private ObservableImpl observable = new AbstractReferenceCountedObservable() {
            @Override
            protected void onAddingFirstObserver() {
                initialized = false;
                binding = data.observe(new Observer() {
                    @Override
                    public void update() {
                        initialized = false;
                        notifyObservers();
                    }
                });
            }

            @Override
            protected void onRemovedLastObserver() {
                binding.close();
                binding = null;
            }
        };

        @Override
        public T getValue() {
            if (binding != null) {
                // use cache only if we are observing
                if (!initialized) {
                    value = data.getValue();
                    initialized = true;
                }
                return value;
            } else {
                return data.getValue();
            }
        }

        @Override
        public Binding observe(Observer observer) {
            return observable.observe(observer);
        }
    }

    /**
     * Wrapper for <code>ObservableData</code> that caches value updates (call of <code>getValue</code> right after notifying the
     * observers)
     * 
     * @param data
     * @return
     */
    public static <T> ObservableData<T> cached(ObservableData<T> data) {
        if (data instanceof CachedObservableData) {
            return data;
        }

        return new CachedObservableData<T>(data);
    }

    public static interface Mapping<T, TResult> {
        public TResult map(T value);
    }

    public static <T, TResult> ObservableData<TResult> map(final ObservableData<? extends T> data,
                                                           final Mapping<? super T, ? extends TResult> mapping) {
        return new ObservableData<TResult>() {

            @Override
            public TResult getValue() {
                return mapping.map(data.getValue());
            }

            @Override
            public Binding observe(Observer observer) {
                return data.observe(observer);
            }
        };
    }

    public static interface Mapping2<T1, T2, TResult> {
        public TResult map(T1 value1, T2 value2);
    }

    public static interface Mapping3<T1, T2, T3, TResult> {
        public TResult map(T1 value1, T2 value2, T3 value3);
    }

    public static <T1, T2, TResult> ObservableData<TResult> map2(final ObservableData<? extends T1> data1,
                                                                  final ObservableData<? extends T2> data2, final Mapping2<? super T1, ? super T2, TResult> mapping) {
        return new ObservableData<TResult>() {
            @Override
            public TResult getValue() {
                return mapping.map(data1.getValue(), data2.getValue());
            }

            @Override
            public Binding observe(Observer observer) {
                return observeMany(new ObservableData<?>[] {
                        data1, data2
                }, observer);
            }
        };
    }

    public static <T1, T2, T3, TResult> ObservableData<TResult> map3(final ObservableData<? extends T1> data1,
                                                                      final ObservableData<? extends T2> data2, final ObservableData<? extends T3> data3,
                                                                      final Mapping3<? super T1, ? super T2, ? super T3, TResult> mapping) {
        return new ObservableData<TResult>() {
            @Override
            public TResult getValue() {
                return mapping.map(data1.getValue(), data2.getValue(), data3.getValue());
            }

            @Override
            public Binding observe(Observer observer) {
                return observeMany(new ObservableData<?>[] {
                        data1, data2, data3
                }, observer);
            }
        };
    }

    /**
     * Convert an indirect data source into a direct one.
     * <p>
     * An indirect data source has an additional indirection allowing to exchange the underlying data source. A default value is
     * returned if the underlying data source is {@code null}.
     */
    public static <T> ObservableData<T> flatten(final ObservableData<? extends ObservableData<T>> indirectDataSource,
                                                final T defaultValue) {
        return new ObservableData<T>() {

            @Override
            public T getValue() {
                ObservableData<T> dataSource = indirectDataSource.getValue();
                if (dataSource != null) {
                    return dataSource.getValue();
                } else {
                    return defaultValue;
                }
            }

            @Override
            public Binding observe(final Observer observer) {
                class UpdaterAndBinding implements Updater<ObservableData<?>>, Binding {
                    public Binding outerBinding;

                    private Binding innerBinding;

                    private boolean active = true;

                    @Override
                    public void update(ObservableData<?> dataSource) {
                        if (innerBinding != null) {
                            innerBinding.close();
                            innerBinding = null;
                        }

                        if ((dataSource != null) && active) {
                            innerBinding = dataSource.observe(observer);
                        }

                        observer.update();
                    }

                    @Override
                    public void close() {
                        active = false;
                        outerBinding.close();

                        if (innerBinding != null) {
                            innerBinding.close();
                            innerBinding = null;
                        }
                    }
                }

                UpdaterAndBinding updaterAndBinding = new UpdaterAndBinding();
                updaterAndBinding.outerBinding = DataBinder.bind(indirectDataSource, updaterAndBinding);

                return updaterAndBinding;
            }
        };
    }
}
