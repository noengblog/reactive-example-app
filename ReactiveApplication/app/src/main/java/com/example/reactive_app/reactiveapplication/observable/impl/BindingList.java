/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable.impl;

import java.util.ArrayList;

import com.example.reactive_app.reactiveapplication.Preconditions;
import com.example.reactive_app.reactiveapplication.observable.Binding;

public class BindingList implements Binding {
    private ArrayList<Binding> bindings = new ArrayList<Binding>();

    public void add(Binding binding) {
        Preconditions.checkNotNull(binding);
        bindings.add(binding);
    }

    @Override
    public void close() {
        for (Binding binding : bindings) {
            binding.close();
        }
        bindings.clear();
    }
}
