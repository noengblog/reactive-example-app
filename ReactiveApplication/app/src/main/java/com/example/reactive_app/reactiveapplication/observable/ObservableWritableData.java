/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable;

public interface ObservableWritableData<T> extends ObservableData<T>, WritableData<T>, Observable {
}
