/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable;

public interface Binding {
    public void close();

    /**
     * Predefined binding object that does nothing on close.
     */
    public final Binding EMPTY = new Binding() {
        @Override
        public void close() {
            // do nothing
        }
    };
}
