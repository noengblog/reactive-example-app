/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable.impl;

import com.example.reactive_app.reactiveapplication.observable.Binding;
import com.example.reactive_app.reactiveapplication.observable.ObservableWritableData;
import com.example.reactive_app.reactiveapplication.observable.Observer;

/**
 * Basic implementation of ObservableData already including the observer management (but without get/set value). It provides a
 * method to notify observers explicitly allowing bridging to other asynchronous mechanisms.
 * 
 * @param <T>
 */
public abstract class AbstractObservableWritableData<T> implements ObservableWritableData<T> {

    private ObservableImpl observable = new ObservableImpl();

    @Override
    public Binding observe(Observer observer) {
        return observable.observe(observer);
    }

    public void notifyObservers() {
        observable.notifyObservers();
    }
}
