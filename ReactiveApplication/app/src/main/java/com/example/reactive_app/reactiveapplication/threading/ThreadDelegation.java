
package com.example.reactive_app.reactiveapplication.threading;

import android.os.Handler;
import android.os.Looper;

public class ThreadDelegation {

    private static Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    /**
     * Direct access to the main thread handler allowing customized posting (e.g. with delay).
     * <p>
     * Note: The instance is always the same (allowing to remove callbacks again).
     * 
     * @return
     */
    public static Handler getMainThreadHandler() {
        return mainThreadHandler;
    }

    /**
     * Check if the given handler forwards to the main thread.
     * 
     * @return
     */
    public static boolean isHandlerOnMainThread(Handler handler) {
        return handler.getLooper() == Looper.getMainLooper();
    }

    /**
     * Runs the given runnable on the main thread. if called from the main thread the runnable is executed immediately.
     * 
     * @param runnable
     */
    public static void runOnMainThread(Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else {
            mainThreadHandler.post(runnable);
        }
    }

    /**
     * Unconditionally schedules the given runnable on the main thread (even if already called from the main thread).
     * <p>
     * In the context of the main thread this may be used to break recursions, deal with reentrance, avoid deadlocks or keep
     * callback executions short.
     * 
     * @param runnable
     */
    public static void scheduleOnMainThread(Runnable runnable) {
        mainThreadHandler.post(runnable);
    }

    /**
     * Forward an exception to the main thread so that the app crashes explicitly.
     */
    public static void postExceptionToMainThread(final Exception e) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * Forward an exception to the main thread so that the app crashes explicitly.
     */
    public static void postExceptionToMainThread(final RuntimeException e) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                throw e;
            }
        });
    }
}
