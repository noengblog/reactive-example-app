/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable;

public interface WritableData<T> extends Data<T> {
    public void setValue(T value);
}
