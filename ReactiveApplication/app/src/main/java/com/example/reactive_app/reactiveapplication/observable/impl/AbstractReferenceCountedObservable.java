/*
 * Copyright (C) 2013 Hilti Corporation, 
 * Liechtenstein (www.hilti.com)
 */

package com.example.reactive_app.reactiveapplication.observable.impl;

import com.example.reactive_app.reactiveapplication.observable.Binding;
import com.example.reactive_app.reactiveapplication.observable.Observer;

public abstract class AbstractReferenceCountedObservable extends ObservableImpl {

    private Object lock = new Object();

    private int numberOfObservers = 0;

    public Binding observe(final Observer observer) {
        synchronized (lock) {
            if (numberOfObservers == 0) {
                onAddingFirstObserver();
            }
            numberOfObservers++;
        }

        final Binding innerBinding = super.observe(observer);

        return new Binding() {
            @Override
            public void close() {
                innerBinding.close();

                synchronized (lock) {
                    numberOfObservers--;
                    if (numberOfObservers == 0) {
                        onRemovedLastObserver();
                    }
                }
            }
        };
    }

    protected abstract void onAddingFirstObserver();

    protected abstract void onRemovedLastObserver();
}
